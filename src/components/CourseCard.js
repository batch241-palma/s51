import { useState } from 'react';


/* ACTIVITY S50 */
import {Button, Card} from 'react-bootstrap';

export default function CourseCard({course}) {
	// const {properties} = course
	const {name, description, price} = course;
	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	/*
		SYNTAX
		const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0);
	/* Activity S51 */
	const [seats, setSeats] = useState(30)
	console.log(useState);

	function enroll() {
		if(seats>0){
		setCount(count + 1);
		setSeats(seats - 1)
		} else(alert("No more seats!"))
	}


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Card.Text>{count} Enrollees</Card.Text>
	        {/*<Button variant="primary">Enroll</Button>*/}
	        <Button className="bg-primary" onClick={enroll}>Enroll</Button>
	    </Card.Body>
	</Card>
	)
}